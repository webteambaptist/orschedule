##If file fails to copy to Sitecore

1. Move file from \\bhpiiswfe07v\d$\SitecoreUpload\COVID-19\Leading Indicators to your local temp folder
2. Move the file back to \\bhpiiswfe07v\d$\SitecoreUpload\COVID-19\Leading Indicators
3. Log into CMS https://ppcm.bh.local/sitecore and verify the file is updated in Media Library folder.
4. Publish the file.
5. Verify it's now updated in the live site

---
