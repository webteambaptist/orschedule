﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NLog;
using ORSchedule.Models;
using System.IO;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using Microsoft.Win32.SafeHandles;

namespace ORSchedule
{
    class Program
    {
        private static readonly Logger OrLogger = NLog.LogManager.GetCurrentClassLogger();

        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private static readonly string OrSchedUserName = ConfigurationManager.AppSettings["User"];
        private static readonly string OrSchedPassword = ConfigurationManager.AppSettings["Pass"];
        private static readonly string OrSchedDomain = ConfigurationManager.AppSettings["Domain"];
        private static readonly string ImportUser = ConfigurationManager.AppSettings["ImportUser"];
        private static readonly string ImportPassword = ConfigurationManager.AppSettings["ImportPassword"];
        private static readonly string OrSchedDirectory = ConfigurationManager.AppSettings["ORFilesIn"];
        private static readonly string OrFiles = ConfigurationManager.AppSettings["ORFilesOut"];
        private const string NewLine = "<br />";
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword,
            int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

        public static void Main(string[] args)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var orFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs\\ORSchedule-{DateTime.Now:MM-dd-yyyy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, orFile);
            LogManager.Configuration = config;
            try
            {
                OrLogger.Info("Starting Logging Service");
                RunProcess();

                Environment.Exit(0);
            }
            catch (Exception e)
            {
                OrLogger.Error("Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + e.Message +
                               NewLine + " ex.InnerException: " + e.InnerException + NewLine);
                var m = new Mail
                {
                    To = To,
                    From = From,
                    Body = "Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + e.Message +
                           NewLine + " ex.InnerException: " + e.InnerException + NewLine + "Full Exception: " + e,
                    Subject = "ORSChedule Process Failed"
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    OrLogger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    OrLogger.Info("Error sending Email");
                }
                OrLogger.Error(e.Message);
            }
        }
        private static void RunProcess()
        {
            try
            {
                //for debugging
                Console.WriteLine("Starting OR File Import...");
                OrLogger.Info("Starting OR File Import...");
                // OR Schedule
                var m = new Mail
                {
                    From = From,
                    To = To,
                    Body = "OR Schedule Process Started....", Subject = "OR Schedule Process Started...."
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("OR Start email sent successfully!");
                    OrLogger.Info("OR Start email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    OrLogger.Error("Error sending Email");
                }
                GetFiles();
                m = new Mail
                {
                    From = From,
                    To = To,
                    Body = "OR Schedule Process Finished...", Subject = "OR Schedule Process Finished..."
                };
                res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("OR Finish email sent successfully!");
                    OrLogger.Info("OR Finish email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    OrLogger.Error("Error sending Email");
                }
                Console.WriteLine("OR File Import Complete!");
                OrLogger.Info("OR File Import Complete!");
                GC.Collect();
            }
            catch(Exception ex)
            {
                OrLogger.Error("Exception occurred while running Program.RunProcess() " + NewLine + " ex.Message: " +
                               ex.Message + NewLine + " ex.InnerException: " + ex.InnerException + NewLine);
                var m = new Mail
                {
                    From = From,
                    To = To,
                    Body = "Exception occurred while running Program.RunProcess() " + NewLine + " ex.Message: " +
                           ex.Message + NewLine + " ex.InnerException: " + ex.InnerException + NewLine +
                           "Full Exception: " + ex,
                    Subject = "ORSchedule Process Failed"
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    OrLogger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    OrLogger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
            }
        }
        private static void GetFiles()
        {
            try
            {
                var copyDir = OrFiles;
                
                if (!Directory.Exists(copyDir))
                {
                    Console.WriteLine("Creating directory, {0}", copyDir);
                    OrLogger.Info("Creating directory, {0}", copyDir);
                    Directory.CreateDirectory(copyDir);
                }

                try
                {
                    try
                    {
                        const int logon32ProviderDefault = 0;
                        //This parameter causes LogonUser to create a primary token.
                        const int logon32LogonInteractive = 2;
                        var returnValue = LogonUser(OrSchedUserName, OrSchedDomain, OrSchedPassword, logon32LogonInteractive,
                            logon32ProviderDefault, out var safeTokenHandle);
                        var text = new StringBuilder();
                        if (false == returnValue)
                        {
                            var ret = Marshal.GetLastWin32Error();
                            throw new System.ComponentModel.Win32Exception(ret);
                        }

                        using (safeTokenHandle)
                        {
                            using (var newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
                            {
                                using (var impersonatedUser = newId.Impersonate())
                                {
                                    var files = Directory.GetFiles(OrSchedDirectory);

                                    foreach (var item in files)
                                    {
                                        var words = item.Split('\\');
                                        if (item.Contains("BMCDOR"))
                                        {
                                            var OrName = "Jacksonville Schedule";
                                            Console.WriteLine(OrName);
                                            OrLogger.Info(OrName);
                                            AddSchedule(copyDir, OrName, OrSchedDirectory, words);
                                        }

                                        if (item.Contains("BMCWOR"))
                                        {
                                            var OrName = "Wolfson Schedule";
                                            Console.WriteLine(OrName);
                                            OrLogger.Info(OrName);
                                            AddSchedule(copyDir, OrName, OrSchedDirectory, words);
                                        }

                                        if (item.Contains("BMCBOR"))
                                        {
                                            var OrName = "Beaches Endoscopy Schedule";
                                            Console.WriteLine(OrName);
                                            OrLogger.Info(OrName);
                                            AddSchedule(copyDir, OrName, OrSchedDirectory, words);
                                        }

                                        if (item.Contains("BMCB2OR"))
                                        {
                                            var OrName = "Beaches OR Schedule";
                                            Console.WriteLine(OrName);
                                            OrLogger.Info(OrName);
                                            AddSchedule(copyDir, OrName, OrSchedDirectory, words);
                                        }

                                        if (item.Contains("bmcnor"))
                                        {
                                            var OrName = "Nassau OR Schedule";
                                            Console.WriteLine(OrName);
                                            OrLogger.Info(OrName);
                                            AddSchedule(copyDir, OrName, OrSchedDirectory, words);
                                        }

                                        if (item.Contains("bmcsor"))
                                        {
                                            var OrName = "South OR Schedule";
                                            Console.WriteLine(OrName);
                                            OrLogger.Info(OrName);
                                            AddSchedule(copyDir, OrName, OrSchedDirectory, words);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        OrLogger.Error("Program/getFiles();  #region tryCatchRegion3" + ex.Message);
                        var m = new Mail
                        {
                            From = From,
                            To = To,
                            Body = "Exception occurred while running Program.getFiles();  #region tryCatchRegion3 " +
                                   NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                                   ex.InnerException + NewLine + "Full Exception: " + ex,
                            Subject = "BHORSChedule Process Failed"
                        };
                        var res = SendMail.SendMailMessage(m);
                        if (res.StatusCode == HttpStatusCode.OK)
                        {
                            Console.WriteLine("Error email sent successfully!");
                            OrLogger.Info("Error email sent successfully!");
                        }
                        else
                        {
                            Console.WriteLine("Error sending Email");
                            OrLogger.Info("Error sending Email");
                        }
                        Console.WriteLine(ex.Message);
                    }

                    // return schedules;
                }
                catch (Exception ex)
                {
                    OrLogger.Error("Program/getFiles();  #region tryCatchRegion2" + ex.Message);
                    var m = new Mail
                    {
                        To = To,
                        From = From,
                        Body = "Exception occurred while running Program.getFiles();  #region tryCatchRegion2 " +
                               NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                               ex.InnerException + NewLine + "Full Exception: " + ex,
                        Subject = "BHORSChedule Process Failed"
                    };
                    var res = SendMail.SendMailMessage(m);
                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Error email sent successfully!");
                        OrLogger.Info("Error email sent successfully!");
                    }
                    else
                    {
                        Console.WriteLine("Error sending Email");
                        OrLogger.Info("Error sending Email");
                    }
                    Console.WriteLine(ex.Message);
                }

                //return schedules; 
            }
            catch (Exception ex)
            {
                OrLogger.Error("Program/getFiles();  #region tryCatchRegion1" + ex.Message);
                var m = new Mail
                {
                    To = To,
                    From = From,
                    Body = "Exception occurred while running Program.getFiles();  #region tryCatchRegion1 " + NewLine +
                           " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " + ex.InnerException +
                           NewLine + "Full Exception: " + ex,
                    Subject = "BHORSChedule Process Failed"
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    OrLogger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    OrLogger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
            }
            
        }

        private static void writeBytesToFile(String fileOutput, byte[] bytes)
        {
            const int logon32ProviderDefault = 0;
            //This parameter causes LogonUser to create a primary token.
            const int logon32LogonInteractive = 2;
            var returnValue = LogonUser(ImportUser, OrSchedDomain, ImportPassword, logon32LogonInteractive,
                logon32ProviderDefault, out var safeTokenHandle);
           
            if (false == returnValue)
            {
                var ret = Marshal.GetLastWin32Error();
                throw new System.ComponentModel.Win32Exception(ret);
            }

            using (safeTokenHandle)
            {
                using (var newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
                {
                    using (var impersonatedUser = newId.Impersonate())
                    {
                        File.WriteAllBytes(fileOutput, bytes);
                    }
                }
            }
        }

        private static void AddSchedule(string copyDir, string scheduleName, string orSchedDirectory, IReadOnlyList<string> words)
        {
            try
            {
                var filename = words[words.Count - 1].ToString();
                byte[] bytes = File.ReadAllBytes(Path.GetFullPath(orSchedDirectory + "\\" + filename));
                writeBytesToFile(copyDir + "\\" + scheduleName + ".txt", bytes);
            }
            catch(Exception ex)
            {
                OrLogger.Error("Program/addSchedule()" +  ex.Message);
                
                Console.WriteLine(ex.Message);
            }
        }
        private sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
        {
            private SafeTokenHandle()
                : base(true)
            {
            }

            [DllImport("kernel32.dll")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            [SuppressUnmanagedCodeSecurity]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool CloseHandle(IntPtr handle);

            protected override bool ReleaseHandle()
            {
                return CloseHandle(handle);
            }
        }

    }
}
